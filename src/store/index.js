import Vue from 'vue'
import Vuex from 'vuex'
import System from './module/System';


Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    System,
  }
})