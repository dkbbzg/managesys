const Domain = 'http://127.0.0.1:3333';
// const Domain = '';

const base = {
	User: Domain + '/user',			   // 用户信息接口
	Goods: Domain + '/goods',			   // 商品接口
}

export default base;