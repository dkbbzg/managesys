import axios from 'axios';
import this_vue from '../main';
let Base64 = require('js-base64').Base64

// 创建axios实例
const instance = axios.create();
// 设置post请求头
instance.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
instance.defaults.headers.get['Content-Type'] = 'application/x-www-form-urlencoded';
instance.defaults.headers.post['Authorization'] = localStorage.getItem('Authorization') ? Base64.decode(localStorage.getItem('Authorization')) : '';
instance.defaults.headers.get['Authorization'] = localStorage.getItem('Authorization') ? Base64.decode(localStorage.getItem('Authorization')) : '';



// 添加请求拦截器
instance.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么
    return config;
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});


// 添加响应拦截器
instance.interceptors.response.use(function (response) {
    if (response.data.code == 400) {
        this_vue.$router.push({ name: "Login" });
    }
    if (response.headers.authorization) {
        localStorage.setItem('Authorization', Base64.encode(response.headers.authorization))
        instance.defaults.headers.post['Authorization'] = localStorage.getItem('Authorization') ? Base64.decode(localStorage.getItem('Authorization')) : '';
        instance.defaults.headers.get['Authorization'] = localStorage.getItem('Authorization') ? Base64.decode(localStorage.getItem('Authorization')) : '';
    }
    return response;
}, function (error) {
    return Promise.reject(error);
});

export default instance;