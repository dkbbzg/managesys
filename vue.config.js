const path = require('path')

function resolve(dir) {
    return path.join(__dirname, dir)
}
module.exports = {
    chainWebpack: config => {
        config.resolve.alias
            .set('@src', resolve('src'))
            .set('@assets', resolve('src/assets'))
            .set('@apis', resolve('src/apis'))
            .set('@components', resolve('src/components'))
            .set('@store', resolve('src/store'))
            .set('@views', resolve('src/views'))
    },
    publicPath: "./"
}